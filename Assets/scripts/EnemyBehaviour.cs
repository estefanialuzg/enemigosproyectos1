using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{

    [SerializeField] float moveSpeed = 1f;

    Rigidbody2D miRigidBody;

    // Start is called before the first frame update
    void Start()
    {
        miRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(IsFacingRight())
        {
            //mover a la derecha
            miRigidBody.velocity = new Vector2(moveSpeed, 0f);
        }else
        {
            //mover a la izquierda
             miRigidBody.velocity = new Vector2(-moveSpeed, 0f);

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //girar
        transform.localScale = new Vector2(-Mathf.Sign(miRigidBody.velocity.x), transform.localScale.y);
    }

    private bool IsFacingRight()
    {
        //Epsilon = un valor muy pequeño
        return transform.localScale.x > Mathf.Epsilon;
    }
}   
